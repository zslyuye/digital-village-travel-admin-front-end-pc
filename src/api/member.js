import request from '@/utils/request'
const Member_URL = '/digital/village/travel/system/member/v1/member'

export function modify(memberDTO) {
  return request({
    url: Member_URL,
    method: 'put',
    data: memberDTO
  })
}

export function commonQuery(memberQuery) {
  return request({
    url: Member_URL,
    method: 'get',
    params: memberQuery
  })
}
