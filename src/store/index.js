import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    user
  },
  getters,
  // 全局数据
  state: {
    loggedUserInfo: {
      'id': '1326429455111028736',
      'type': '0',
      'name': '安抚',
      'code': 'anfu',
      'password': '123456',
      'permisson': 'http://img.58cdn.com.cn/ershouxc/pc/revision/index/license/license.jpg',
      'boss': '孙佳佳',
      'tel': '123456789',
      'email': '12345678@qq.com',
      'address': '中国河南三门峡',
      'tenantId': '1',
      'orgId': '1',
      'companyId': '1',
      'createdBy': '1',
      'creator': '1',
      'createdTime': '2020-12-29 17:48:18',
      'updatedBy': '1',
      'modifier': '1',
      'updatedTime': '2020-12-29 17:48:23',
      'status': 1,
      'version': '0'
    }
  },
  // 修改state中的全局数据的唯一接口，mutations的函数只能是同步函数
  mutations: {
    setLoggedUserInfo(state, userInfo) {
      this.state.loggedUserInfo = userInfo
    }
  },
  // 修改state中的全局数据的间接接口，可以为异步函数，通过调用mutations中的函数实现对state的修改
  actions: {

  }
})

export default store
