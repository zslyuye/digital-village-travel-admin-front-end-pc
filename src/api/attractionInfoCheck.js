import request from '@/utils/request'

const ATTRACTION_URL = '/digital/village/travel/system/platform/basedata/v1/attraction'

export function commonQuery(attractionQuery) {
  return request({
    url: ATTRACTION_URL + '/pc',
    method: 'get',
    params: attractionQuery
  })
}

export function modify(attractionDTO) {
  return request({
    url: ATTRACTION_URL,
    method: 'put',
    data: attractionDTO
  })
}

export function queryAttractionById(id) {
  return request({
    url: ATTRACTION_URL + '/' + id,
    method: 'get'
  })
}

export function batchUpdate(attractionDTOS) {
  return request({
    url: ATTRACTION_URL + '/batch',
    method: 'put',
    data: attractionDTOS
  })
}
