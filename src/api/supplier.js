import request from '@/utils/request'
const Supplier_URL = '/digital/village/travel/system/platform/basedata/v1/supplier'

export function add(addTemporaryDTO) {
  return request({
    url: Supplier_URL,
    method: 'post',
    data: addTemporaryDTO
  })
}

export function modify(supplierDTO) {
  return request({
    url: Supplier_URL,
    method: 'put',
    data: supplierDTO
  })
}

export function commonQuery(supplierQuery) {
  return request({
    url: Supplier_URL,
    method: 'get',
    params: supplierQuery
  })
}

export function remove(id, version) {
  return request({
    url: Supplier_URL,
    method: 'delete',
    params: {
      id: id,
      version: version
    }
  })
}

export function check(checkSupplierDTO) {
  return request({
    url: Supplier_URL + '/check',
    method: 'put',
    data: checkSupplierDTO
  })
}
