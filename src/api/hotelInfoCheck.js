import request from '@/utils/request'

const HOTEL_URL = '/digital/village/travel/system/platform/basedata/v1/hotel/info'

export function commonQuery(hotelQuery) {
  return request({
    url: HOTEL_URL + '/pc',
    method: 'get',
    params: hotelQuery
  })
}

export function modify(hotelDTO) {
  return request({
    url: HOTEL_URL,
    method: 'put',
    data: hotelDTO
  })
}

export function queryHotelById(id) {
  return request({
    url: HOTEL_URL + '/' + id,
    method: 'get'
  })
}

export function batchUpdate(hotelDTOS) {
  return request({
    url: HOTEL_URL + '/batch',
    method: 'put',
    data: hotelDTOS
  })
}
